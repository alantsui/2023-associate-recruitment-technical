SELECT owner.owner_id as owner_id, owner_name, count(distinct hello.category_id) as different_category_count
FROM (
    SELECT owner_id, category_id
    FROM ((article
        inner join owner on owner.owner_id = article.owner_id)
        inner join category_article_mapping on article.article_id = category_article_mapping.article_id)
    ) "hello" inner join owner on owner.owner_id = hello.owner_id
GROUP by owner_id
ORDER BY different_category_count DESC